package cngroup.application;

import cngroup.mock.Assignments;
import cngroup.model.Assignment;

import java.util.*;

public class ClassicApproach {

    public static void main(String[] args) {
        System.out.println("Hello basic");

        List<Assignment> assignments  = Assignments.ASSIGNMENTS_1;

        // Create mapByKeys
        Map<String, List<Assignment>> mapByKeys = new HashMap<>();
        for (Assignment assignment: assignments) {
            List list = mapByKeys.get(assignment.getKeyId());
            if (list != null) {
                list.add(assignment);
            } else {
                mapByKeys.put(assignment.getKeyId(), new ArrayList<>(Arrays.asList(assignment)));
            }
        }

    }
}
