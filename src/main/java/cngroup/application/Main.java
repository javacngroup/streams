package cngroup.application;

import cngroup.mock.Assignments;
import cngroup.model.Assignment;

import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.groupingBy;

public class Main {

    public static void main(String[] args) {
        System.out.println("Hello Streams!");
        List<Assignment> assignments  = Assignments.ASSIGNMENTS_1;

        // Create mapByKeys from assignments
        Map<String, List<Assignment>> mapByKeys =  assignments.stream()
                .collect(groupingBy(Assignment::getKeyId));
 /*
        //1. Detect biggest/highest key
        String generalMasterKey = ?;

        // remove generalMasterKey
        mapByKeys.?;

        //2. Detect central cylinder, everything what has > 2 X in one row

        // revert mapByKeys back to List<Assignment>
        List<Assignment> assignmentsWithoutGeneralMaster = collectMapToAssignments(mapByKeys);

        // Create mapByCylinders from assignments
        Map<String, List<Assignment>> mapByCylinders = ?;

        // detect centralCylinders - everything what has > 2 X in one row
        List<String> centralCylinders = ?;

        //3. Detect own keys/E - key with one X in vertical line and not considering centralCylinders/generalMasterKey

        //remove all centralCylinders from mapByCylinders
        centralCylinders.?;

        // revert mapByCylinders back to List<Assignment>
        List<Assignment> assignmentsWithoutGeneralMasterAndCentralCylinders = collectMapToAssignments(mapByCylinders);

        // Create mapByKeys2 from assignments
        Map<String, List<Assignment>> mapByKeys2 = ?;

        // detect ownKeysAssignments and collect them to List<Assignment> - key with one X in vertical line
        List<Assignment> ownKeysAssignments = mapByKeys2.entrySet().stream().?;

        // set type OWNED
        ownKeysAssignments.?;
        */
    }

    private static List<Assignment> collectMapToAssignments(Map<String, List<Assignment>> assignmentsMap) {
        // TODO convert assignmentsMap to list
        return null;
    }
}
