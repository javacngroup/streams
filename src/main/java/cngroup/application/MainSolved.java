package cngroup.application;

import cngroup.mock.Assignments;
import cngroup.model.Assignment;

import java.util.*;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

public class MainSolved {

    public static void main(String[] args) {
        System.out.println("Hello Streams SOLVED!");

        List<Assignment> assignments  = Assignments.ASSIGNMENTS_1;

    // Create mapByKeys from assignments
        Map<String, List<Assignment>> mapByKeys = assignments.stream()
                .collect(groupingBy(Assignment::getKeyId));

    // 1. Detect biggest/highest key
        String generalMasterKey = mapByKeys.entrySet().stream()
                .filter(e -> (e.getValue().size() > 1))
                .max(Comparator.comparingInt(e -> e.getValue().size()))
                .get().getKey();

        // remove generalMasterKey
        mapByKeys.remove(generalMasterKey);

    // 2. Detect central cylinder, everything what has > 2 X in one row

        // revert mapByKeys back to List<Assignment>
        List<Assignment> assignmentsWithoutGeneralMaster = collectMapToAssignments(mapByKeys);

        // Create mapByCylinders from assignments
        Map<String, List<Assignment>> mapByCylinders = assignmentsWithoutGeneralMaster.stream()
                .collect(groupingBy(Assignment::getCylinderId));

        // detect centralCylinders - everything what has > 2 X in one row
        List<String> centralCylinders = mapByCylinders.entrySet().stream()
                .filter(e -> e.getValue().size() > 2)
                .map(Map.Entry::getKey)
                .collect(toList());

     // 3. Detect own keys/E - key with one X in vertical line and not considering centralCylinders/generalMasterKey

        //remove all centralCylinders from mapByCylinders
        centralCylinders.forEach(s -> {mapByCylinders.remove(s);});

        // revert mapByCylinders back to List<Assignment>
        List<Assignment> assignmentsWithoutGeneralMasterAndCentralCylinders = collectMapToAssignments(mapByCylinders);

        // Create mapByKeys2 from assignments
        Map<String, List<Assignment>> mapByKeys2 = assignmentsWithoutGeneralMasterAndCentralCylinders.stream()
                .collect(groupingBy(Assignment::getKeyId));

        // detect ownKeysAssignments and collect them to List<Assignment> - key with one X in vertical line
        List<Assignment> ownKeysAssignments = mapByKeys2.entrySet().stream()
                .filter(e -> e.getValue().size() == 1)
                .map(Map.Entry::getValue)
                .flatMap(List::stream)
                .collect(toList());

        // set type OWNED
        ownKeysAssignments.forEach(assignment -> assignment.setType(Assignment.Type.OWNED));
    }

    private static List<Assignment> collectMapToAssignments(Map<String, List<Assignment>> assignmentsMap) {
        return assignmentsMap
                .values()
                .stream()
                .flatMap(List::stream)
                .collect(toList());
    }
}
