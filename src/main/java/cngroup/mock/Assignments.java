package cngroup.mock;

import cngroup.model.Assignment;
import cngroup.model.Cylinder;
import cngroup.model.Key;

import java.util.Arrays;
import java.util.List;

import static cngroup.mock.Cylinders.CYLINDERS_1;
import static cngroup.mock.Keys.KEYS_1;

public class Assignments {

    public static final List<Assignment> ASSIGNMENTS_1;

    static {
        ASSIGNMENTS_1 = Arrays.asList(
            assignment(CYLINDERS_1.get(0), KEYS_1.get(0)),
            assignment(CYLINDERS_1.get(1), KEYS_1.get(1)),
            assignment(CYLINDERS_1.get(2), KEYS_1.get(2)),
            assignment(CYLINDERS_1.get(3), KEYS_1.get(3)),
            assignment(CYLINDERS_1.get(0), KEYS_1.get(4)),
            assignment(CYLINDERS_1.get(1), KEYS_1.get(4)),
            assignment(CYLINDERS_1.get(2), KEYS_1.get(4)),
            assignment(CYLINDERS_1.get(3), KEYS_1.get(4)),
            assignment(CYLINDERS_1.get(2), KEYS_1.get(1)),
            assignment(CYLINDERS_1.get(2), KEYS_1.get(0))
        );
    }


    private static Assignment assignment(Cylinder cylinder, Key key) {
        Assignment a = new Assignment();
        a.setCylinderId(cylinder.getId());
        a.setKeyId(key.getId());
        return a;
    }

}
