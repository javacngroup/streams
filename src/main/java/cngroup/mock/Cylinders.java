package cngroup.mock;

import cngroup.model.Cylinder;

import java.util.Arrays;
import java.util.List;

class Cylinders {

    static final List<Cylinder> CYLINDERS_1;

    static {
        Cylinder c1 = new Cylinder();
        c1.setId("1");
        c1.setName("Cylinder1");
        c1.setLengthInside(21.0);
        c1.setLengthOutside(21.0);
        c1.setColor("NI");
        c1.setOwnKeyCount(0);
        c1.setCentral(true);

        Cylinder c2 = new Cylinder();
        c2.setId("2");
        c2.setName("Cylinder2");
        c2.setLengthInside(25.0);
        c2.setLengthOutside(25.0);
        c2.setColor("MS");
        c2.setOwnKeyCount(0);
        c2.setCentral(false);

        Cylinder c3 = new Cylinder();
        c3.setId("3");
        c3.setName("Cylinder3");
        c3.setLengthInside(null);
        c3.setLengthOutside(null);
        c3.setColor(null);
        c3.setOwnKeyCount(0);
        c3.setCentral(false);

        Cylinder c4 = new Cylinder();
        c4.setId("4");
        c4.setName("Cylinder4");
        c4.setLengthInside(null);
        c4.setLengthOutside(null);
        c4.setColor(null);
        c4.setOwnKeyCount(0);
        c4.setCentral(false);

        CYLINDERS_1 = Arrays.asList(c1, c2, c3, c4);
    }


}
