package cngroup.mock;

import cngroup.model.Key;

import java.util.Arrays;
import java.util.List;

class Keys {
    static final List<Key> KEYS_1;

    static {
        Key k1 = new Key();
        k1.setId("A");
        k1.setName("KeyA");
        k1.setDescription(null);
        k1.setProfile(null);

        Key k2 = new Key();
        k2.setId("B");
        k2.setName("KeyB");
        k2.setDescription(null);
        k2.setProfile(null);

        Key k3 = new Key();
        k3.setId("C");
        k3.setName("KeyC");
        k3.setDescription(null);
        k3.setProfile(null);

        Key k4 = new Key();
        k4.setId("D");
        k4.setName("KeyD");
        k4.setDescription(null);
        k4.setProfile(null);

        Key k5 = new Key();
        k5.setId("E");
        k5.setName("KetE");
        k5.setDescription(null);
        k5.setProfile(null);

        KEYS_1 = Arrays.asList(k1, k2, k3, k4, k5);
    }

}
