package cngroup.model;

public class Assignment {

    private String cylinderId;
    private String keyId;

    private Type type;

    public String getCylinderId() {
        return cylinderId;
    }

    public void setCylinderId(String cylinderId) {
        this.cylinderId = cylinderId;
    }

    public String getKeyId() {
        return keyId;
    }

    public void setKeyId(String keyId) {
        this.keyId = keyId;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public enum Type {
        OWNED("E"), UNLOCKS("X");

        private final String value;

        Type(String value) {
            this.value = value;
        }
    }

}
