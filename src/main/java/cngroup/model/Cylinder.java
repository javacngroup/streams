package cngroup.model;

public class Cylinder {

    private String id;
    private Double lengthInside;
    private Double lengthOutside;
    private String name;
    private String color;
    private Integer ownKeyCount;
    private String ownKeyId;
    private boolean central;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getLengthInside() {
        return lengthInside;
    }

    public void setLengthInside(Double lengthInside) {
        this.lengthInside = lengthInside;
    }

    public Double getLengthOutside() {
        return lengthOutside;
    }

    public void setLengthOutside(Double lengthOutside) {
        this.lengthOutside = lengthOutside;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Integer getOwnKeyCount() {
        return ownKeyCount;
    }

    public void setOwnKeyCount(Integer ownKeyCount) {
        this.ownKeyCount = ownKeyCount;
    }

    public String getOwnKeyId() {
        return ownKeyId;
    }

    public void setOwnKeyId(String ownKeyId) {
        this.ownKeyId = ownKeyId;
    }

    public boolean isCentral() {
        return central;
    }

    public void setCentral(boolean central) {
        this.central = central;
    }


}
