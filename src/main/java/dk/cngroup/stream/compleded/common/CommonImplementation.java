package dk.cngroup.stream.compleded.common;

import java.util.List;
import java.util.stream.Stream;

public interface CommonImplementation {
    Stream<Person> getResult(List<Person> personList);
}
