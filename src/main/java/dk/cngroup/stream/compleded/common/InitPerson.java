package dk.cngroup.stream.compleded.common;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class InitPerson {
    public static List<Person> init() {

        Person person1 = new Person();
        person1.setId(1);
        person1.setName("Peter");
        person1.setLastName("Kapusnik");
        person1.setBirdDate(new Date());
        person1.setConutry("SK");

        Person person2 = new Person();
        person2.setId(1);
        person2.setName("Lukas");
        person2.setLastName("Spalek");
        person2.setBirdDate(new Date());
        person2.setConutry("SK");

        Person person3 = new Person();
        person3.setId(1);
        person3.setName("Lukas");
        person3.setLastName("Kerammikar");
        person3.setBirdDate(new Date());
        person3.setConutry("CZ");

        return Arrays.asList(person1, person2, person3);
    }
}
