package dk.cngroup.stream.compleded.common;

import java.util.Date;

public class Person {

    private int id;
    private String name;
    private String lastName;
    private Date birdDate;
    private String country;

    public Person() {
    }

    public Person(String name, String lastName) {
        this.name = name;
        this.lastName = lastName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirdDate() {
        return birdDate;
    }

    public void setBirdDate(Date birdDate) {
        this.birdDate = birdDate;
    }

    public String getConutry() {
        return country;
    }

    public void setConutry(String conutry) {
        this.country = country;
    }
}
