package dk.cngroup.stream.completed.runnable;

import dk.cngroup.stream.compleded.common.InitPerson;
import dk.cngroup.stream.compleded.common.Person;

import java.util.List;

public class AnyMatch {

    public static void main(String[] args) {

        List<Person> personList = new InitPerson().init();

        boolean matched = personList.stream()
                .anyMatch(person -> person.getLastName().startsWith("K"));

        System.out.println(matched);

    }
}
