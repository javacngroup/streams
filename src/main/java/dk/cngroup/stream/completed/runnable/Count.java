package dk.cngroup.stream.completed.runnable;

import dk.cngroup.stream.compleded.common.InitPerson;
import dk.cngroup.stream.compleded.common.Person;

import java.util.List;

public class Count {
    public static void main(String[] args) {

        List<Person> personList = new InitPerson().init();
        long numberOfPersons = personList.stream()
                .filter(person -> person.getLastName().startsWith("K"))
                .count();

        System.out.println("Number of count Persons : " + numberOfPersons + ".");
    }
}


