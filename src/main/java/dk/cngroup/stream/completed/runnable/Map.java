package dk.cngroup.stream.completed.runnable;

import dk.cngroup.stream.compleded.common.CommonImplementation;
import dk.cngroup.stream.compleded.common.InitPerson;
import dk.cngroup.stream.compleded.common.Person;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class Map implements CommonImplementation {

    private Object Person;

    public static void main(String[] args) {

        CommonImplementation instance = new Map();

        List<Person> personList = new InitPerson().init();
        instance.getResult(personList).forEach(person -> System.out.println(person.getName().concat(" ").concat(person.getLastName())));
    }

    @Override
    public Stream<Person> getResult(List<Person> personList) {

        List<String> list = personList.stream().filter(person -> person.getLastName().startsWith("K")).map(person -> person.getLastName().toUpperCase()).collect(Collectors.toList());

        Stream<Person> personName = list.stream().map((lastName) -> new Person("", lastName));
        return personName;

    }
}

