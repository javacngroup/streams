package dk.cngroup.stream.solution;

import java.util.stream.Stream;
import java.util.Date;

public class DStreamIterate {

    public static void main(String[] args) {
        Stream<Date> stream = Stream.generate(() -> {
            return new Date();
        });
        stream.forEach(p -> System.out.println(p));
    }

}