package dk.cngroup.stream.solution;

import dk.cngroup.stream.compleded.common.CommonImplementation;
import dk.cngroup.stream.compleded.common.InitPerson;
import dk.cngroup.stream.compleded.common.Person;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class GStreamFilter implements CommonImplementation {
    public static void main(String[] args) {

        GStreamFilter instance = new GStreamFilter();

        List<Person> persons= new InitPerson().init();
        instance.getResult(persons)
            .forEach(person -> System.out.println(person));
    }

    @Override
    public Stream getResult(List<Person> personList) {
        return personList.stream().filter(Predicate.isEqual("K"));
    }
}
