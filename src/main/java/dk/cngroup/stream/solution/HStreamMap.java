package dk.cngroup.stream.solution;

public class HStreamMap {
    public static void main(String[] args) {
        SetMemberNames memberNames = new SetMemberNames();


        memberNames.memberNames.stream().filter((s) -> s.startsWith("A"))
                .map(String::toUpperCase)
                .forEach(System.out::println);


    }
}
