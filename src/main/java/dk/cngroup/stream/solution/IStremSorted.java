package dk.cngroup.stream.solution;

public class IStremSorted {

    public static void main(String[] args) {

        SetMemberNames memberNames = new SetMemberNames();

        memberNames.memberNames.stream().sorted()
                .map(String::toUpperCase)
                .forEach(System.out::println);
    }
}



