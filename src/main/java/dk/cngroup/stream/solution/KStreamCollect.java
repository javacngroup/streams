package dk.cngroup.stream.solution;

import java.util.List;
import java.util.stream.Collectors;

public class KStreamCollect {
    public static void main(String[] args) {

        SetMemberNames memberNames = new SetMemberNames();

        List<String> memNamesInUppercase = memberNames.memberNames.stream().sorted()
                .map(String::toUpperCase)
                .collect(Collectors.toList());

        System.out.print(memNamesInUppercase);


    }
}
