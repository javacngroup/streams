package dk.cngroup.stream.solution;

public class LStreamMatch {
    public static void main(String[] args) {
        SetMemberNames memberNames = new SetMemberNames();


        boolean matchedResult = memberNames.memberNames.stream()
            .anyMatch((s) -> s.startsWith("A"));

        System.out.println(matchedResult);
        matchedResult = memberNames.memberNames.stream()
            .allMatch((s) -> s.startsWith("A"));

        System.out.println(matchedResult);
        matchedResult = memberNames.memberNames.stream()
            .noneMatch((s) -> s.startsWith("A"));

        System.out.println(matchedResult);
    }

}
