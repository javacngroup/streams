package dk.cngroup.stream.solution;

public class NStreamCount {

    public static void main(String[] args) {
        SetMemberNames memberNames = new SetMemberNames();

        long totalMatched = memberNames.memberNames.stream()
                .filter((s) -> s.startsWith("A"))
                .count();
        System.out.println(totalMatched);


    }

}
