package dk.cngroup.stream.solution;

import java.util.Optional;

public class OStreamReduce {

    public static void main(String[] args) {

        SetMemberNames memberNames = new SetMemberNames();

        Optional<String> reduced = memberNames.memberNames
                .stream()
                .reduce((s1, s2) -> s1 + "#" + s2);
        reduced.ifPresent(System.out::println);
    }

}
