package dk.cngroup.stream.solution;

public class PStreamAnyMatch {
    public static void main(String[] args) {

        SetMemberNames memberNames = new SetMemberNames();

        boolean matched = memberNames.memberNames.stream()
                .anyMatch((s) -> s.startsWith("A"));
        System.out.println(matched);

    }
}
