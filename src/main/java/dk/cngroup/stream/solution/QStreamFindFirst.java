package dk.cngroup.stream.solution;

public class QStreamFindFirst {

    public static void main(String[] args) {

        SetMemberNames memberNames = new SetMemberNames();

        String firstMatchedName = memberNames.memberNames.stream()
                .filter((s) -> s.startsWith("A"))
                .findFirst().get();
        System.out.println(firstMatchedName);

    }

}
