package dk.cngroup.stream.solution;

import java.util.ArrayList;
import java.util.stream.Stream;

public class RStreamParalerizm {
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<Integer>();

        for (int i = 1;
             i < 10;
             i++) {
            list.add(i);
        }

        // Here crated paraller stream
        Stream<Integer> stream = list.parallelStream();
        Integer[] evenNumbersArr = stream.filter(i -> i % 2 == 0).toArray(Integer[]::new);
        System.out.print(evenNumbersArr);

    }
}
