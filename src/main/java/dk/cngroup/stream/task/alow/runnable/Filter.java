package dk.cngroup.stream.task.alow.runnable;

import dk.cngroup.stream.compleded.common.CommonImplementation;
import dk.cngroup.stream.compleded.common.InitPerson;
import dk.cngroup.stream.compleded.common.Person;

import java.util.List;
import java.util.stream.Stream;

public class Filter implements CommonImplementation {
    public static void main(String[] args) {

        CommonImplementation instance = new FindFirst();

        List<Person> personList = new InitPerson().init();
        instance.getResult(personList).forEach(person -> person.getLastName());

    }

    @Override
    public Stream<Person> getResult(List<Person> personList) {
        // Return all persons for for Last name starting with "K"
        // ....
        // ....
        return null;
    }
}
