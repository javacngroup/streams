package dk.cngroup.stream.task.alow.runnable;

import dk.cngroup.stream.compleded.common.CommonImplementation;
import dk.cngroup.stream.compleded.common.InitPerson;
import dk.cngroup.stream.compleded.common.Person;

import java.util.List;
import java.util.stream.Stream;

public class FindFirst implements CommonImplementation {
    public static void main(String[] args) {

        CommonImplementation instance = new FindFirst();

        List<Person> personList = new InitPerson().init() ;
        instance.getResult(personList).forEach(person -> System.out.println(person.getName().concat(" ").concat(person.getLastName())));
    }

    @Override
    public Stream<Person> getResult(List<Person> personList) {
        // Return first person for Last name starting with "S"
        // ....
        // ....
        return null;
    }
}
