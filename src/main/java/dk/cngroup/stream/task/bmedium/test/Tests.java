package dk.cngroup.stream.task.bmedium.test;

import dk.cngroup.stream.compleded.common.Person;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class Tests {

    @Test
    public void CountTest() {
        long count = getPersonList().stream().filter(person -> person.getLastName().startsWith("K")).count();
        Assert.assertEquals(count, 2);
    }

    @Test
    public void AnyMatchTest() {
       boolean anyMatch =  getPersonList().stream().anyMatch(person -> person.getLastName().startsWith("K"));
       Assert.assertEquals(true, true);
    }

    private List<Person> getPersonList() {

        // Old Person list
        Person person1 = new Person();
        person1.setId(1);
        person1.setName("Peter");
        person1.setLastName("Kapusnik");
        person1.setBirdDate(new Date());
        person1.setConutry("SK");

        Person person2 = new Person();
        person2.setId(1);
        person2.setName("Lukas");
        person2.setLastName("Spalek");
        person2.setBirdDate(new Date());
        person2.setConutry("SK");

        Person person3 = new Person();
        person3.setId(1);
        person3.setName("Lukas");
        person3.setLastName("Kerammikar");
        person3.setBirdDate(new Date());
        person3.setConutry("CZ");

        return Arrays.asList(person1, person2, person3);

    }

}
