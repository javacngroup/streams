

import dk.cngroup.stream.compleded.common.InitPerson;
import dk.cngroup.stream.compleded.common.Person;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class Tests {

    @Test
    public void FilterTest() {

        // Streams
        Stream<Person> oldPersonStream = getPersonList().stream().filter(person -> person.getLastName().startsWith("K"));
        Stream<Person> newPersonStream = new InitPerson().init().stream().filter(person -> person.getLastName().startsWith("K"));

        Assert.assertEquals(oldPersonStream.allMatch(Predicate.isEqual(new Person())),
                newPersonStream.allMatch(Predicate.isEqual(new Person())));

    }

    @Test
    public void FindFirstTest() {

        // Streams
        Stream<Person> oldPersonStream = getPersonList().stream().filter((person) -> person.getLastName().startsWith("S"));
        Stream<Person> newPersonStream = new InitPerson().init().stream().filter((person) -> person.getLastName().startsWith("S"));

        Assert.assertEquals(oldPersonStream.allMatch(Predicate.isEqual(new Person())),
                newPersonStream.allMatch(Predicate.isEqual(new Person())));

    }

    @Test
    public void MapTest() {

    }

    @Test
    public void SortedTest() {

    }

    private List<Person> getPersonList() {

        // Old Person list
        Person person1 = new Person();
        person1.setId(1);
        person1.setName("Peter");
        person1.setLastName("Kapusnik");
        person1.setBirdDate(new Date());
        person1.setConutry("SK");

        Person person2 = new Person();
        person2.setId(1);
        person2.setName("Lukas");
        person2.setLastName("Spalek");
        person2.setBirdDate(new Date());
        person2.setConutry("SK");

        Person person3 = new Person();
        person3.setId(1);
        person3.setName("Lukas");
        person3.setLastName("Kerammikar");
        person3.setBirdDate(new Date());
        person3.setConutry("CZ");

        return Arrays.asList(person1, person2, person3);

    }


}
